<?php

/**
 * @file
 * Bulk export of ctypes generated by Drush Ctools Export Bonus.
 * Use drush cbrct modulename to rebuild ctypes.
 */

/**
 * Implements hook_drush_ctex_bonus_ctypes().
 */
function ccss_views_drush_ctex_bonus_ctypes() {
  $ctypes = array();

  $ctypes['page'] = array(
    'type' => 'page',
    'name' => 'Static Page',
    'base' => 'node_content',
    'module' => 'node',
    'description' => 'Use <em>static pages</em> for your static content, such as an \'About us\' page.',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'disabled' => '0',
    'orig_type' => 'page',
    'disabled_changed' => FALSE,
  );

  $ctypes['presenter'] = array(
    'type' => 'presenter',
    'name' => 'Presenter',
    'base' => 'node_content',
    'module' => 'node',
    'description' => '',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Name',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'disabled' => '0',
    'orig_type' => 'presenters',
    'disabled_changed' => FALSE,
  );

  $ctypes['resource'] = array(
    'type' => 'resource',
    'name' => 'Resource',
    'base' => 'node_content',
    'module' => 'node',
    'description' => 'A resource is one or more files or links to Common Core resources.',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'disabled' => '0',
    'orig_type' => 'resource',
    'disabled_changed' => FALSE,
  );

  $ctypes['workshop'] = array(
    'type' => 'workshop',
    'name' => 'Workshop',
    'base' => 'node_content',
    'module' => 'node',
    'description' => '',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Title',
    'custom' => '1',
    'modified' => '1',
    'locked' => '0',
    'disabled' => '0',
    'orig_type' => 'workshop',
    'disabled_changed' => FALSE,
  );

  return $ctypes;
}
